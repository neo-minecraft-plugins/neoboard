package io.neonlightning.neoboard;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();

        player.sendMessage(ChatColor.GREEN + "Welcome To The Server: " + ChatColor.DARK_PURPLE + player.getDisplayName());

        //FastBoard board = new FastBoard(player);

        //board.updateTitle(ChatColor.RED + "FastBoard");

        //boards.put(player.getUniqueId(), board);
    }

}
